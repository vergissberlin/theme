const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const tar = require('gulp-tar-path');
const gzip = require('gulp-gzip');
const clean = require('gulp-clean');
const cleanCSS = require('gulp-clean-css');
const hash = require('gulp-hash-filename');
const replace = require('gulp-replace');

const sources = [
    'archetypes',
    'i18n',
    'layouts',
    'static',
    'DCO',
    'LICENSE',
    'README.md',
    'theme.toml'
];

gulp.task('default', ['sass']);
gulp.task('prod', ['sass-prod'])

gulp.task('release', () => {
    return gulp.src(sources)
        .pipe(tar('master.tar'))
        .pipe(gzip())
        .pipe(gulp.dest('dist'))
});

gulp.task('sass', () => {
    return gulp.src('./src/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./static/styles'));
});

gulp.task('sass-prod', () => {
    return gulp.src('./src/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS())
        .pipe(replace('../static', '..'))
        .pipe(hash())
        .pipe(gulp.dest('./static/styles'));
})

gulp.task('clean', () => {
    return gulp.src(['dist', 'static/styles'])
        .pipe(clean());
});

gulp.task('watch', () => {
    return gulp.watch('./src/*.scss', ['sass']);
});
